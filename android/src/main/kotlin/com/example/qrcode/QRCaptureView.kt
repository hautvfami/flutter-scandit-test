package com.example.qrcode

import android.Manifest
import android.app.Activity
import android.app.Application
import android.content.pm.PackageManager.PERMISSION_GRANTED
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import com.journeyapps.barcodescanner.BarcodeView
import com.scandit.datacapture.barcode.capture.BarcodeCapture
import com.scandit.datacapture.barcode.capture.BarcodeCaptureListener
import com.scandit.datacapture.barcode.capture.BarcodeCaptureSettings
import com.scandit.datacapture.barcode.capture.SymbologySettings
import com.scandit.datacapture.barcode.data.Symbology
import com.scandit.datacapture.core.capture.DataCaptureContext
import com.scandit.datacapture.core.source.Camera
import com.scandit.datacapture.barcode.capture.BarcodeCaptureSession;
import com.scandit.datacapture.barcode.data.Barcode;
import com.scandit.datacapture.barcode.ui.overlay.BarcodeCaptureOverlay;
import com.scandit.datacapture.core.data.FrameData;
import com.scandit.datacapture.core.ui.viewfinder.RectangularViewfinder;
import com.scandit.datacapture.core.source.FrameSourceState
import com.scandit.datacapture.core.ui.DataCaptureView
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.PluginRegistry
import io.flutter.plugin.platform.PlatformView


class QRCaptureView(private val registrar: PluginRegistry.Registrar, id: Int) :
        PlatformView, MethodCallHandler, BarcodeCaptureListener {


    private var scanditView: DataCaptureView? = null;
    private var camera: Camera? = null;
    private var barcodeCapture: BarcodeCapture? = null;
    private var dataCaptureContext: DataCaptureContext? = null;
    private var symbologies: HashSet<Symbology> = HashSet();

    //    private var barcodeView: BarcodeView? = null
    private val activity = registrar.activity()
    var cameraPermissionContinuation: Runnable? = null
    private var requestingPermission = false
    private val channel: MethodChannel

    private val licenseKey = "ARUfzheBCCN7N/tpzgkRhhsd22dFN7WN9mlBgaB/+08XFZgnKWGPor5hGk3mcFYqlUCjID9NQamzUlRPH3e0bvpzJe9Ja2606m9jIPtWCg1IRGObt3m6BD8fj7j5d8gzmR18bkoaColYHnitoiUjK6UAS/einZkVPRZflEtE3eyl34ylBn0KT3u6nla1IV6WbSK3Fdj9JpafpA+AwulwvjAm2hoFJF2jxW0mYXabhfqCdRuDItPGjiS9ccrQdERnBlXGQbp0WKj87RKn2rozD7JQlFUwHXlZTejNuvHUHkL65w2z9MirgFlSItlQvtzWmY3JB4Fkg/w0kyvmC9s/qAI9hW+NAbateAEGbtyW38TTWfLD97CnKIEY3yqgtvYdB2UfsE/qcD6dV1if25UeKyFVAFrdGzfcQ4R1IiEYl+1SPWXEAiPQNQQKKi+bC8AYvwEc3e2uOS2lWdn0xcGehbVuyIAJIpE8qkvDlUHWeYFu5QudVz1NwsAA+fUdSqEAqng9LH9gmEfE0Oas9zUHoqZTHbXyV6ZHEKZM8it+ZkHpICkSIMlmxlBDZ9P9BMQ106ogkiw5IdkAfmVGaNEq1Q4hCODwbxFEOdsHHV9cHiYXxcKeyfrOeqgymbmnT934laoSgW3MdHgh6SuplfEmAmI0OO42KxM9+Odd/KklTnXgn7u+MbxcEsbHnNy4lFrnhXcI3UCEIb2e9tFkAokPXUqhz7aJX3WTTdtuj1DbnVgP5jC3PhEaywEwmBMejOFPb8dRHAb2MtoiLxR0ft3wHk4zfuK09HUEsuIxc/NGJHc6eo4n+FnAp2/KTvZPHpNsWA==";


    override fun onMethodCall(call: MethodCall, result: MethodChannel.Result) {
        when (call?.method) {
            "checkAndRequestPermission" -> {
                checkAndRequestPermission(result)
            }
        }

        when (call?.method) {
            "resume" -> {
//                resume()
            }
        }

        when (call?.method) {
            "pause" -> {
//                pause()
            }
        }

        when (call?.method) {
            "setTorchMode" -> {
//                val isOn = call.arguments as Boolean
//                barcodeView?.setTorch(isOn);
            }
        }
    }

    private fun resume() {
//        barcodeView?.resume()
    }

    private fun pause() {
//        barcodeView?.pause()
    }

    private fun checkAndRequestPermission(result: MethodChannel.Result?) {
        if (cameraPermissionContinuation != null) {
            result?.error("cameraPermission", "Camera permission request ongoing", null);
        }

        cameraPermissionContinuation = Runnable {
            cameraPermissionContinuation = null
            if (!hasCameraPermission()) {
                result?.error(
                        "cameraPermission", "MediaRecorderCamera permission not granted", null)
                return@Runnable
            }
        }

        requestingPermission = false
        if (hasCameraPermission()) {
            cameraPermissionContinuation?.run()
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestingPermission = true
                registrar
                        .activity()
                        .requestPermissions(
                                arrayOf(Manifest.permission.CAMERA),
                                CAMERA_REQUEST_ID)
            }
        }
    }


    private fun hasCameraPermission(): Boolean {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M ||
                activity.checkSelfPermission(Manifest.permission.CAMERA) == PERMISSION_GRANTED
    }

    override fun onBarcodeScanned(barcodeCapture: BarcodeCapture, session: BarcodeCaptureSession, data: FrameData) {
        super.onBarcodeScanned(barcodeCapture, session, data)
        session.newlyRecognizedBarcodes.firstOrNull()?.let {
            Log.e("QRCapture", it.data);
        }
    }
    companion object {
        const val CAMERA_REQUEST_ID = 513469796
    }

    init {
        registrar.addRequestPermissionsResultListener(CameraRequestPermissionsListener())
        channel = MethodChannel(registrar.messenger(), "plugins/qr_capture/method_$id")
        channel.setMethodCallHandler(this)
        checkAndRequestPermission(null)

//        val barcode = BarcodeView(registrar.activity())
//        this.barcodeView = barcode

        symbologies?.add(Symbology.EAN13_UPCA)
        symbologies?.add(Symbology.CODE128)
        dataCaptureContext = DataCaptureContext.Companion.forLicenseKey("!")
        camera = Camera.getDefaultCamera();
        print(camera?.toString())
        if (camera != null) {
            camera?.switchToDesiredState(FrameSourceState.ON);
            camera?.applySettings(BarcodeCapture.createRecommendedCameraSettings())
            dataCaptureContext?.setFrameSource(camera)
        } else {
            print("Camera null________")
        }

        val barcodeCaptureSettings = BarcodeCaptureSettings()
        barcodeCaptureSettings.enableSymbologies(this.symbologies);
        val symbologySettings: SymbologySettings = barcodeCaptureSettings.getSymbologySettings(Symbology.CODE39)

//        symbologySettings.setActiveSymbolCounts(activeSymbolCounts);

        // Create new barcode capture mode with the settings from above.
        barcodeCapture = BarcodeCapture.forDataCaptureContext(dataCaptureContext, barcodeCaptureSettings);
        barcodeCapture?.addListener(this)


        this.scanditView = DataCaptureView.newInstance(context = registrar.activity(), dataCaptureContext = dataCaptureContext)

//        barcode.decodeContinuous(
//                object : BarcodeCallback {
//                    override fun barcodeResult(result: BarcodeResult) {
//                        channel.invokeMethod("onCaptured", result.text)
//                    }
//
//                    override fun possibleResultPoints(resultPoints: List<ResultPoint>) {}
//                }
//        )
//
//        barcode.resume()

        registrar.activity().application.registerActivityLifecycleCallbacks(
                object : Application.ActivityLifecycleCallbacks {
                    override fun onActivityPaused(p0: Activity?) {
                        if (p0 == registrar.activity()) {
//                            barcodeView?.pause()
                        }
                    }

                    override fun onActivityResumed(p0: Activity?) {
                        if (p0 == registrar.activity()) {
//                            barcodeView?.resume()
                        }
                    }

                    override fun onActivityStarted(p0: Activity?) {
                    }

                    override fun onActivityDestroyed(p0: Activity?) {
                    }

                    override fun onActivitySaveInstanceState(p0: Activity?, p1: Bundle?) {
                    }

                    override fun onActivityStopped(p0: Activity?) {
                    }

                    override fun onActivityCreated(p0: Activity?, p1: Bundle?) {
                    }

                }
        )
    }

    override fun getView(): View {
        return this.scanditView!!;
//        return this.barcodeView!!;
    }

    override fun dispose() {
//        barcodeView?.pause()
//        barcodeView = null
    }

    private inner class CameraRequestPermissionsListener : PluginRegistry.RequestPermissionsResultListener {
        override fun onRequestPermissionsResult(id: Int, permissions: Array<String>, grantResults: IntArray): Boolean {
            if (id == CAMERA_REQUEST_ID && grantResults[0] == PERMISSION_GRANTED) {
                cameraPermissionContinuation?.run()
                return true
            }
            return false
        }
    }

}
